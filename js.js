$("#top").height($(window).height() * 0.10);
$("#bottom").height($(window).height() * 0.7);
var toggleI = 0,
	mode = "0",
	evenOdd = 0,
	minClr = 1,
	clrMode = false,
	randOptVis = false,
	curMode = 0,
	counter = 0,
	interval = 10,
	sel = "",
	color = ["#000", "#fff"],
	curColor = ["#000", "#fff"],
	canvas = $("#canvas"),
	velXorig = 0,
	velYorig = 0,
	ctx = canvas[0].getContext("2d"),
	width = $("#bottom").innerWidth(),
	height = $("#bottom").innerHeight(),
	box ={x:0,y:0,oldX:0,oldY:0,radius:8,color:curColor[0],velX:0,velY:0,trueVel:25,deg:0,rad:0};
ctx.canvas.width = width;
ctx.canvas.height = height;
function draw(name, x, y, radius){
	ctx.beginPath();
	ctx.fillStyle = name.color;
	ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
	ctx.fill();
}
function line(name, x1, y1, x2, y2){
	ctx.strokeStyle = name.color;
	ctx.lineWidth = name.radius * 2;
	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.stroke();
}
function square(name, x, y){
	ctx.fillStyle = name.color;
	ctx.fillRect(x, y, name.radius*2, name.radius*2);
}
function clear(){
	ctx.clearRect(0,0,width,height);
	box.x = 0;
	box.y = 0;
	box.oldX = 0;
	box.oldY = 0;
	box.color = curColor[0];
	counter = 0;
	toggleI = 0;
	ctx.beginPath();
	ctx.moveTo(0,0);
	ctx.lineTo(0,0);
	ctx.stroke();
}
function angle(angIn){
	curColor = color.slice(0);
	curMode = parseInt(mode.substr(0));
	if(Number.isNaN(angIn) || angIn == null){
		box.deg = Math.round(Math.random() * 3600)/10;
		$("#deg").val(box.deg);
	} else {
		box.deg = angIn;
	}
	clear();
	updateSel();
	box.rad = box.deg * Math.PI / 180;
	if(curMode != 2){
		interval = 10;
		box.velX = box.trueVel * Math.sin(box.rad);
		box.velY = box.trueVel * Math.cos(box.rad);
	} else {
		interval = 1;
		box.velX = Math.abs(box.trueVel * Math.sin(box.rad));
		box.velY = Math.abs(box.trueVel * Math.cos(box.rad));
	}
	velXorig = box.velX;
	velYorig = box.velY;
}
angle();
function toggleColor(){
	toggleI++;
	if(toggleI < curColor.length){
		box.color = curColor[toggleI];
	} else {
		toggleI = 0;
		box.color = curColor[toggleI];
	}
}
function updateSel(){
	$("#mode").val(parseInt(mode.substr(0)));
	$("#clrstr").val(color);
	$("evenOdd").val(evenOdd);
	$("#min").val(minClr);
	opt = $("#colorSel option");
	if(color.length>opt.length){
		for(var i=opt.length; i<color.length; i++){
			$("#colorSel").append(new Option(i, i));
		}
	} else{
		for(var i=color.length; i<opt.length; i++){
			opt[i].remove();
		}
	}
	$("#clr").val(color[$("#colorSel").prop('selectedIndex')]);
	sel = $("#colorSel").prop('selectedIndex');
}
$("#mode").change(function(){
	mode = $("#mode").val();
})
$("#colorSel").change(function(){
	updateSel();
})
$("#clr").change(function(){
	color[sel] = $("#clr").val();
	$("#clrstr").val(color);
})
$("#new").click(function(){
	$("#colorSel").append(new Option(color.length, color.length));
	$("#colorSel").val(color.length);
	color.push("");
	updateSel();
	return false;
})
$("#del").click(function(){
	opt = $("#colorSel option");
	if(opt.length > 1){
		$("#colorSel").val(opt.length-2);
		opt[--opt.length].remove();
		color.pop();
		updateSel();
		return false;
	}
})
$("#clrstr").change(function(){
	color = $("#clrstr").val().split(",");
	updateSel();
})
$("#clrbtn").click(function(){
	$(".colorSel, #clrstr, #morerandclr").toggle()
	clrMode = !clrMode;
	return false;
})
function goToAng(){
	try{
		n = eval($("#deg").val());
		if(!Number.isNaN(n)){
			angNum = n;
		}
	}	catch(e){
		angNum = null;
	}
	angle(angNum);
}
function addRandtoColor(){
	x = Math.floor(Math.random()*4095).toString(16);
	while(x.length<3){
		x = "0"+x;
	}
	color.push("#"+x);
}
$("#randclr").click(function(){
	if(clrMode == false){
		x = Math.floor(Math.random()*4095).toString(16);
		while(x.length<3){
			x = "0"+x;
		}
		color[sel] = "#"+x.toString(16);
		$("#clr").val(color[$("#colorSel").prop("selectedIndex")]);
	} else{
		m = true;
		color = [];
		start = Math.round(minClr/((evenOdd>=1)? 2 : 1))-((evenOdd>=1)? 0 : 1);
		for(var i = 0; i < start; i++){
			addRandtoColor();
		}
		if(evenOdd == 2){
			addRandtoColor();
		}
		while(m){
			addRandtoColor();
			if(evenOdd >=1){
				addRandtoColor();
			}
			m = Math.random() < ((evenOdd>=1)? .25 : .5);
		}
		goToAng();
	}
	$("#clrstr").val(color);
	return false;
})
$("#morerandclr").click(function(){
	randOptVis = !randOptVis;
	if(randOptVis == true){
		$("#moreOpt").css({"display": "block", 
			"top":$("#text").position()["top"]-$("#moreOpt").height(), 
			"left":$("#morerandclr").position()["left"]-$("#text").position()["left"]
		});
	} else {
		$("#moreOpt").css("display", "none");
	}
	return false;
})
$("#evenOdd").change(function(){
	evenOdd = parseInt($("#evenOdd").val());
})
$("#min").change(function(){
	minClr = parseInt($("#min").val());
})
$("#min").focusout(function(){
	if(evenOdd > 0){
		minClr =　Math.round(2*(parseInt($("#min").val())+((evenOdd == 1) ? 0:1)))/2;
	}
	$("min").val(minClr);
})
$("#go").click(function(){
	goToAng();
	return false;
})
$("#random").click(function(){
	angle();
	return false;
})
var intervalFn = function(){
	//draw
	if(curMode==0){
		draw(box, box.x, box.y, box.radius);
		line(box, box.oldX, box.oldY, box.x, box.y);
	}else if (curMode==1) {
		square(box, box.x, box.y);
	}
	if(box.x > 0 || box.y > 0 ){
		if(counter == 0){
			counter++
		}
	}
	if(Math.round(box.x/10) == 0 && Math.round(box.y/10) == 0 && counter == 1){
		curMode = -1;
	}
	if(curMode == -1){}
	else if(curMode == 0 || curMode == 1){
		//Horizontal Movement + Limits
		box.oldX = box.x;
		if (box.x < 0){
			toggleColor();
			box.velX = -box.velX;
			box.x = 0;
		}else if (box.x+box.radius > width){
			toggleColor();
			box.velX = -box.velX;
			box.x = width-box.radius;
		}else{
			box.x -= box.velX;
		}
		//Verticle Movement + Limits
		box.oldY = box.y;
		if(box.y < 0){
			toggleColor();
			box.velY = -box.velY;
			box.y = 0;
		}else if (box.y+box.radius > height){
			toggleColor();
			box.velY = -box.velY;
			box.y = height-box.radius;
		}else {
			box.y += box.velY;
		}
	}else if(curMode == 2){
		box.oldX = box.x;
		box.oldY = box.y;
		incrX = (box.velX > 0)? ((width - box.x) / box.velX):(-box.x/ box.velX);
		incrY = (box.velY > 0)? ((height - box.y) / box.velY):(-box.y / box.velY);
		if(incrX > incrY){
			box.y = (box.velY > 0)? height:0;
			box.x += box.velX * incrY;
			box.velY = -box.velY;
		}else {
			box.x = (box.velX > 0)? width:0
			box.y += box.velY * incrX;
			box.velX = -box.velX;
		}
		line(box, box.oldX, box.oldY, box.x, box.y);
		toggleColor();
	}
	setTimeout(intervalFn, interval);
}
setTimeout(intervalFn, interval);
