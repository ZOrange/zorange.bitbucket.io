function ix(ip){
	var t=[];
	for(var i=0;i<ip.length;i++){
		t.push(i);
	}
	return t;
}
function sh(a,b){
	var m=a.length,i,t,u;
	while(m){	
		i=Math.floor(Math.random()*m--);
		t=a[m];
		a[m]=a[i];
		a[i]=t;
		if(b!=null){
			u=b[m]
			b[m]=b[i];
			b[i]=u;
		}
	}
	return a;
}
function u(a,k){
	var t=[]
	for(var i=0;i<a.length;i++){
		t[k[i]]=a[i]
	}
	return t;
}
function ia(a){
	var t=[];
	for(var i=0;i<a.length;i++){
		t[a[i]]=i;
	}
	return t;
}
var eI="",basea=[0,1,2,3,4,5,6,7,8,9],ra=[],ira=[],outk=[],enc;
function e(ip){
	var p=[],n="",ra=[],ir=[],s=[],k=[];
	for(var i=0;i<ip.length;i++){
		p=ip.charCodeAt(i).toString(10);
		while(p.length<3){p="0"+p;}
		for(var j=0;j<p.length;j++){
			ra=sh(basea);
			ir=ia(ra);
			n+=ra[p.charAt(j)];
			k.push(ir);
		}
	}
	s=ix(n);
	n=sh(n.split(""),s).join("");
	this.outk="[["+k.join("],[")+"],["+s+"]]";
	this.encOut=n;
}
function d(ip,k){
	var p=[],q="",out="";
	ip=u(ip.split(""),k[k.length-1]).join("");
	for(var i=0;i<ip.length;i++){
		p.push(k[i][parseInt(ip.charAt(i),10)]);
	}
	for(var j=0;j<ip.length/3;j++){
		q="";
		for(var k=0;k<3;k++){
			q+=p.shift();
		}
		out+=String.fromCharCode(q);
	}
	return out;
}
$("#encLink").click(function(){
	eI=$("#encIn").val();
	if(eI!=""){
		enc = new e(eI);
		$("#encOut").val(enc.encOut);
		$("#keyOut").val(enc.outk);
	}
});
$("#decLink").click(function(){
	decIn=$("#decIn").val();
	if(decIn!=""){
		kIn=$("#keyIn").val();
		if(kIn!=""){
			kIn = JSON.parse(kIn);
			$("#decOut").val(d(decIn,kIn));
		}
	}
});