var theight = $("nav").height()+$("header").height()
  vrange = $("body").height()-theight,
  y = 0,
  x = -50,
  direction = "left",
  running = false,
  rand = 0;
setInterval(function(){
  if(running == false){
    rand = Math.random();
    if(rand<0.1){
      running = true;
      y = theight+Math.round(vrange * Math.random());
      if(rand<0.05){
        direction = "right"
        $("#fireball").css("transform", "scaleX(1)");
      } else{
        direction = "left";
        $("#fireball").css("transform", "scaleX(-1)");
        x = $("body").width();
      }
      $("#fireball").css("filter","hue-rotate(" + ((Math.random() < 0.5) ? 0 : 200) + "deg)");
    }
  }
}, 1000)
setInterval(function(){
  if(running == true){
    if(direction == "right"){
      x += 2;
      dy = 25*Math.sin(Math.PI/75*x)+y;
      $("#fireball").css({"left": x+"px", "top": dy+"px"});
    } else {
      x -= 2;
      dy = 25*Math.sin(Math.PI/75*x)+y;
      $("#fireball").css({"left": x+"px", "top": dy+"px"});
    }
    if((direction == "right") ? x>$("body").width() : x<-50){
      $("#fireball").css({"left": "-50px", "top": "-50px"});
      running = false;
    }
  }
}, 10)
